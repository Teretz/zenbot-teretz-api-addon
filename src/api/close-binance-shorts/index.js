import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-binance-shorts Create close binance shorts
 * @apiName CreateCloseBinanceShorts
 * @apiGroup CloseBinanceShorts
 * @apiSuccess {Object} closeBinanceShorts Close binance shorts's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close binance shorts not found.
 */
router.post('/',
  create)

export default router
