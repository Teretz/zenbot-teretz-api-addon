import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /stop-binance Create stop binance
 * @apiName CreateStopBinance
 * @apiGroup StopBinance
 * @apiSuccess {Object} stopBinance Stop binance's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Stop binance not found.
 */
router.post('/',
  create)

export default router
