import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-yfi-down Create close yfi down
 * @apiName CreateCloseYfiDown
 * @apiGroup CloseYfiDown
 * @apiSuccess {Object} closeYfiDown Close yfi down's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close yfi down not found.
 */
router.post('/',
  create)

export default router
