import { Router } from 'express'
import zenbot from './zenbot'
import processLong from './processLong'
import processShort from './processShort'
import processLong25 from './processLong25'
import processShort25 from './processShort25'

import cancelOrders from './cancelOrders'
import processLongsDev from './processLongsDev'
import openBinanceShorts from './open-binance-shorts'
import openBinanceLongs from './open-binance-longs'
import closeBinanceShorts from './close-binance-shorts'
import closeBinanceLongs from './close-binance-longs'
import startBinance from './start-binance'
import stopBinance from './stop-binance'
import reverseBinanceLongs from './reverse-binance-longs'
import reverseBinanceShorts from './reverse-binance-shorts'
import openXrpUp from './open-xrp-up'
import closeXrpUp from './close-xrp-up'
import openAaveUp from './open-aave-up'
import closeAaveUp from './close-aave-up'
import openDotUp from './open-dot-up'
import closeDotUp from './close-dot-up'
import openAaveDown from './open-aave-down'
import closeAaveDown from './close-aave-down'
import openXrpDown from './open-xrp-down'
import closeXrpDown from './close-xrp-down'
import openDotDown from './open-dot-down'
import closeDotDown from './close-dot-down'
import trendspider from './trendspider'
import storeOpenAaveUp from './store-open-aave-up'
import storeOpenLong from './store-open-long'
import openEthUp from './open-eth-up'
import openEthDown from './open-eth-down'
import closeEthUp from './close-eth-up'
import openYfiUp from './open-yfi-up'
import openYfiDown from './open-yfi-down'
import closeYfiUp from './close-yfi-up'
import closeYfiDown from './close-yfi-down'
import openAdaUp from './open-ada-up'
import openAdaDown from './open-ada-down'
import closeAdaDown from './close-ada-down'
import closeAdaUp from './close-ada-up'
import closeEthDown from './close-eth-down'



const router = new Router()

router.use('/processLong', zenbot)
router.use('/processLongs', processLong)
router.use('/processShorts', processShort)
router.use('/processLongs25', processLong25)
router.use('/processShorts25', processShort25)

router.use('/cancelOrders', cancelOrders)
router.use('/processLongsDev', processLongsDev)
router.use('/open-binance-shorts', openBinanceShorts)
router.use('/open-binance-longs', openBinanceLongs)
router.use('/close-binance-shorts', closeBinanceShorts)
router.use('/close-binance-longs', closeBinanceLongs)
router.use('/start-binance', startBinance)
router.use('/stop-binance', stopBinance)
router.use('/reverse-binance-longs', reverseBinanceLongs)
router.use('/reverse-binance-shorts', reverseBinanceShorts)
router.use('/open-xrp-up', openXrpUp)
router.use('/close-xrp-up', closeXrpUp)
router.use('/open-aave-up', openAaveUp)
router.use('/close-aave-up', closeAaveUp)
router.use('/open-dot-up', openDotUp)
router.use('/close-dot-up', closeDotUp)
router.use('/open-aave-down', openAaveDown)
router.use('/cloe-aave-down', closeAaveDown)
router.use('/open-xrp-down', openXrpDown)
router.use('/close-xrp-down', closeXrpDown)
router.use('/open-dot-down', openDotDown)
router.use('/close-dot-down', closeDotDown)
router.use('/trendspider', trendspider)
router.use('/store-open-aave-up', storeOpenAaveUp)
router.use('/store-open-long', storeOpenLong)
router.use('/open-eth-up', openEthUp)
router.use('/open-eth-down', openEthDown)
router.use('/close-eth-up', closeEthUp)
router.use('/open-yfi-up', openYfiUp)
router.use('/open-yfi-down', openYfiDown)
router.use('/close-yfi-up', closeYfiUp)
router.use('/close-yfi-down', closeYfiDown)
router.use('/open-ada-up', openAdaUp)
router.use('/open-ada-down', openAdaDown)
router.use('/close-ada-down', closeAdaDown)
router.use('/close-ada-up', closeAdaUp)
router.use('/open-eth-down', openEthDown)
router.use('/close-eth-down', closeEthDown)
router.use('/open-eth-up', openEthUp)
router.use('/close-eth-up', closeEthUp)



/**
 * @apiDefine master Master access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine admin Admin access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine user User access only
 * You must pass `access_token` parameter or a Bearer Token authorization header
 * to access this endpoint.
 */
/**
 * @apiDefine listParams
 * @apiParam {String} [q] Query to search.
 * @apiParam {Number{1..30}} [page=1] Page number.
 * @apiParam {Number{1..100}} [limit=30] Amount of returned items.
 * @apiParam {String[]} [sort=-createdAt] Order of returned items.
 * @apiParam {String[]} [fields] Fields to be returned.
 */

export default router
