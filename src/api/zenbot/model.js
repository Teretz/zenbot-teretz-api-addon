import mongoose, { Schema } from 'mongoose'

const zenbotSchema = new Schema({}, { timestamps: true })

zenbotSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Zenbot', zenbotSchema)

export const schema = model.schema
export default model
