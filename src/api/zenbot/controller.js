import { success, notFound } from '../../services/response/'
import { Zenbot } from '.'

export const create = ({ body }, res, next) =>
  Zenbot.create(body)
    .then((zenbot) => zenbot.view(true))
    .then(success(res, 201))
    .catch(next)
