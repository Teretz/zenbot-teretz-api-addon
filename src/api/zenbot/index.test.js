import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { Zenbot } from '.'

const app = () => express(apiRoot, routes)

let zenbot

beforeEach(async () => {
  zenbot = await Zenbot.create({})
})

test('POST /processLong 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
})
