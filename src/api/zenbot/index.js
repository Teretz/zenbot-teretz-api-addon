import { Router } from 'express'
import { create } from './controller'
export Zenbot, { schema } from './model'

const router = new Router()

/**
 * @api {post} /processLong Create zenbot
 * @apiName CreateZenbot
 * @apiGroup Zenbot
 * @apiSuccess {Object} zenbot Zenbot's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Zenbot not found.
 */
router.post('/',
  create)

export default router
