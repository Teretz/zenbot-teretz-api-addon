import { Zenbot } from '.'

let zenbot

beforeEach(async () => {
  zenbot = await Zenbot.create({})
})

describe('view', () => {
  it('returns simple view', () => {
    const view = zenbot.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(zenbot.id)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = zenbot.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(zenbot.id)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
