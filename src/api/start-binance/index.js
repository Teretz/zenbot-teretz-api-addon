import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /start-binance Create start binance
 * @apiName CreateStartBinance
 * @apiGroup StartBinance
 * @apiSuccess {Object} startBinance Start binance's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Start binance not found.
 */
router.post('/',
  create)

export default router
