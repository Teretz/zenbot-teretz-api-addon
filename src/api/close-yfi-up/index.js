import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-yfi-up Create close yfi up
 * @apiName CreateCloseYfiUp
 * @apiGroup CloseYfiUp
 * @apiSuccess {Object} closeYfiUp Close yfi up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close yfi up not found.
 */
router.post('/',
  create)

export default router
