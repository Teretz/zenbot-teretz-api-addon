import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /processShorts Create process short
 * @apiName CreateProcessShort
 * @apiGroup ProcessShort
 * @apiSuccess {Object} processShort Process short's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Process short not found.
 */
router.post('/',
  create)

export default router
