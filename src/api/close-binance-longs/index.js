import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-binance-longs Create close binance longs
 * @apiName CreateCloseBinanceLongs
 * @apiGroup CloseBinanceLongs
 * @apiSuccess {Object} closeBinanceLongs Close binance longs's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close binance longs not found.
 */
router.post('/',
  create)

export default router
