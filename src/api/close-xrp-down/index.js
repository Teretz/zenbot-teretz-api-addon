import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-xrp-down Create close xrp down
 * @apiName CreateCloseXrpDown
 * @apiGroup CloseXrpDown
 * @apiSuccess {Object} closeXrpDown Close xrp down's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close xrp down not found.
 */
router.post('/',
  create)

export default router
