import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-binance-shorts Create open binance shorts
 * @apiName CreateOpenBinanceShorts
 * @apiGroup OpenBinanceShorts
 * @apiSuccess {Object} openBinanceShorts Open binance shorts's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open binance shorts not found.
 */
router.post('/',
  create)

export default router
