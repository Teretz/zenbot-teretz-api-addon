import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-dot-up Create close dot up
 * @apiName CreateCloseDotUp
 * @apiGroup CloseDotUp
 * @apiSuccess {Object} closeDotUp Close dot up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close dot up not found.
 */
router.post('/',
  create)

export default router
