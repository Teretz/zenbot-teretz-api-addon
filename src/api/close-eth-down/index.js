import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-eth-down Create close eth down
 * @apiName CreateCloseEthDown
 * @apiGroup CloseEthDown
 * @apiSuccess {Object} closeEthDown Close eth down's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close eth down not found.
 */
router.post('/',
  create)

export default router
