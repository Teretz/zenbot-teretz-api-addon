import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-xrp-up Create close xrp up
 * @apiName CreateCloseXrpUp
 * @apiGroup CloseXrpUp
 * @apiSuccess {Object} closeXrpUp Close xrp up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close xrp up not found.
 */
router.post('/',
  create)

export default router
