import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /reverse-binance-shorts Create reverse binance shorts
 * @apiName CreateReverseBinanceShorts
 * @apiGroup ReverseBinanceShorts
 * @apiSuccess {Object} reverseBinanceShorts Reverse binance shorts's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Reverse binance shorts not found.
 */
router.post('/',
  create)

export default router
