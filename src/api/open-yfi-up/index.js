import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-yfi-up Create open yfi up
 * @apiName CreateOpenYfiUp
 * @apiGroup OpenYfiUp
 * @apiSuccess {Object} openYfiUp Open yfi up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open yfi up not found.
 */
router.post('/',
  create)

export default router
