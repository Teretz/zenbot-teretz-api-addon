import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-dot-down Create open dot down
 * @apiName CreateOpenDotDown
 * @apiGroup OpenDotDown
 * @apiSuccess {Object} openDotDown Open dot down's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open dot down not found.
 */
router.post('/',
  create)

export default router
