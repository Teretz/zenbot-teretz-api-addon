import { StoreOpenLong } from '.'

let storeOpenLong

beforeEach(async () => {
  storeOpenLong = await StoreOpenLong.create({ exchange: 'test', symbol: 'test', action: 'test', percent: 'test', status: 'test', message: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = storeOpenLong.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(storeOpenLong.id)
    expect(view.exchange).toBe(storeOpenLong.exchange)
    expect(view.symbol).toBe(storeOpenLong.symbol)
    expect(view.action).toBe(storeOpenLong.action)
    expect(view.percent).toBe(storeOpenLong.percent)
    expect(view.status).toBe(storeOpenLong.status)
    expect(view.message).toBe(storeOpenLong.message)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = storeOpenLong.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(storeOpenLong.id)
    expect(view.exchange).toBe(storeOpenLong.exchange)
    expect(view.symbol).toBe(storeOpenLong.symbol)
    expect(view.action).toBe(storeOpenLong.action)
    expect(view.percent).toBe(storeOpenLong.percent)
    expect(view.status).toBe(storeOpenLong.status)
    expect(view.message).toBe(storeOpenLong.message)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
