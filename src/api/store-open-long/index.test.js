import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { StoreOpenLong } from '.'

const app = () => express(apiRoot, routes)

let storeOpenLong

beforeEach(async () => {
  storeOpenLong = await StoreOpenLong.create({})
})

test('POST /store-open-long 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ exchange: 'test', symbol: 'test', action: 'test', percent: 'test', status: 'test', message: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.exchange).toEqual('test')
  expect(body.symbol).toEqual('test')
  expect(body.action).toEqual('test')
  expect(body.percent).toEqual('test')
  expect(body.status).toEqual('test')
  expect(body.message).toEqual('test')
})

test('GET /store-open-long 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}`)
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /store-open-long/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`${apiRoot}/${storeOpenLong.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(storeOpenLong.id)
})

test('GET /store-open-long/:id 404', async () => {
  const { status } = await request(app())
    .get(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /store-open-long/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`${apiRoot}/${storeOpenLong.id}`)
    .send({ exchange: 'test', symbol: 'test', action: 'test', percent: 'test', status: 'test', message: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(storeOpenLong.id)
  expect(body.exchange).toEqual('test')
  expect(body.symbol).toEqual('test')
  expect(body.action).toEqual('test')
  expect(body.percent).toEqual('test')
  expect(body.status).toEqual('test')
  expect(body.message).toEqual('test')
})

test('PUT /store-open-long/:id 404', async () => {
  const { status } = await request(app())
    .put(apiRoot + '/123456789098765432123456')
    .send({ exchange: 'test', symbol: 'test', action: 'test', percent: 'test', status: 'test', message: 'test' })
  expect(status).toBe(404)
})

test('DELETE /store-open-long/:id 204', async () => {
  const { status } = await request(app())
    .delete(`${apiRoot}/${storeOpenLong.id}`)
  expect(status).toBe(204)
})

test('DELETE /store-open-long/:id 404', async () => {
  const { status } = await request(app())
    .delete(apiRoot + '/123456789098765432123456')
  expect(status).toBe(404)
})
