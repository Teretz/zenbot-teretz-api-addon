import mongoose, { Schema } from 'mongoose'

const storeOpenLongSchema = new Schema({
  exchange: {
    type: String
  },
  symbol: {
    type: String
  },
  action: {
    type: String
  },
  percent: {
    type: String
  },
  status: {
    type: String
  },
  message: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

storeOpenLongSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      exchange: this.exchange,
      symbol: this.symbol,
      action: this.action,
      percent: this.percent,
      status: this.status,
      message: this.message,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('StoreOpenLong', storeOpenLongSchema)

export const schema = model.schema
export default model
