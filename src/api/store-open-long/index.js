import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy } from './controller'
import { schema } from './model'
export StoreOpenLong, { schema } from './model'

const router = new Router()
const { exchange, symbol, action, percent, status, message } = schema.tree

/**
 * @api {post} /store-open-long Create store open long
 * @apiName CreateStoreOpenLong
 * @apiGroup StoreOpenLong
 * @apiParam exchange Store open long's exchange.
 * @apiParam symbol Store open long's symbol.
 * @apiParam action Store open long's action.
 * @apiParam percent Store open long's percent.
 * @apiParam status Store open long's status.
 * @apiParam message Store open long's message.
 * @apiSuccess {Object} storeOpenLong Store open long's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Store open long not found.
 */
router.post('/',
  body({ exchange, symbol, action, percent, status, message }),
  create)

/**
 * @api {get} /store-open-long Retrieve store open longs
 * @apiName RetrieveStoreOpenLongs
 * @apiGroup StoreOpenLong
 * @apiUse listParams
 * @apiSuccess {Object[]} storeOpenLongs List of store open longs.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query(),
  index)

/**
 * @api {get} /store-open-long/:id Retrieve store open long
 * @apiName RetrieveStoreOpenLong
 * @apiGroup StoreOpenLong
 * @apiSuccess {Object} storeOpenLong Store open long's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Store open long not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /store-open-long/:id Update store open long
 * @apiName UpdateStoreOpenLong
 * @apiGroup StoreOpenLong
 * @apiParam exchange Store open long's exchange.
 * @apiParam symbol Store open long's symbol.
 * @apiParam action Store open long's action.
 * @apiParam percent Store open long's percent.
 * @apiParam status Store open long's status.
 * @apiParam message Store open long's message.
 * @apiSuccess {Object} storeOpenLong Store open long's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Store open long not found.
 */
router.put('/:id',
  body({ exchange, symbol, action, percent, status, message }),
  update)

/**
 * @api {delete} /store-open-long/:id Delete store open long
 * @apiName DeleteStoreOpenLong
 * @apiGroup StoreOpenLong
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Store open long not found.
 */
router.delete('/:id',
  destroy)

export default router
