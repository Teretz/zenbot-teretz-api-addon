import { success, notFound } from '../../services/response/'
import { StoreOpenLong } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  StoreOpenLong.create(body)
    .then((storeOpenLong) => storeOpenLong.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) =>
  StoreOpenLong.find(query, select, cursor)
    .then((storeOpenLongs) => storeOpenLongs.map((storeOpenLong) => storeOpenLong.view()))
    .then(success(res))
    .catch(next)

export const show = ({ params }, res, next) =>
  StoreOpenLong.findById(params.id)
    .then(notFound(res))
    .then((storeOpenLong) => storeOpenLong ? storeOpenLong.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  StoreOpenLong.findById(params.id)
    .then(notFound(res))
    .then((storeOpenLong) => storeOpenLong ? Object.assign(storeOpenLong, body).save() : null)
    .then((storeOpenLong) => storeOpenLong ? storeOpenLong.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  StoreOpenLong.findById(params.id)
    .then(notFound(res))
    .then((storeOpenLong) => storeOpenLong ? storeOpenLong.remove() : null)
    .then(success(res, 204))
    .catch(next)
