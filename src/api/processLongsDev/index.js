import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /processLongsDev Create process longs dev
 * @apiName CreateProcessLongsDev
 * @apiGroup ProcessLongsDev
 * @apiSuccess {Object} processLongsDev Process longs dev's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Process longs dev not found.
 */
router.post('/',
  create)

export default router
