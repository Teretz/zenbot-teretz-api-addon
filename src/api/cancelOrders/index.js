import { Router } from 'express'
import { create, destroy } from './controller'

const router = new Router()

/**
 * @api {post} /cancelOrders Create cancel orders
 * @apiName CreateCancelOrders
 * @apiGroup CancelOrders
 * @apiSuccess {Object} cancelOrders Cancel orders's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Cancel orders not found.
 */
router.post('/',
  create)

/**
 * @api {delete} /cancelOrders/:id Delete cancel orders
 * @apiName DeleteCancelOrders
 * @apiGroup CancelOrders
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Cancel orders not found.
 */
router.delete('/:id',
  destroy)

export default router
