import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /reverse-binance-longs Create reverse binance longs
 * @apiName CreateReverseBinanceLongs
 * @apiGroup ReverseBinanceLongs
 * @apiSuccess {Object} reverseBinanceLongs Reverse binance longs's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Reverse binance longs not found.
 */
router.post('/',
  create)

export default router
