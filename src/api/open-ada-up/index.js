import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-ada-up Create open ada up
 * @apiName CreateOpenAdaUp
 * @apiGroup OpenAdaUp
 * @apiSuccess {Object} openAdaUp Open ada up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open ada up not found.
 */
router.post('/',
  create)

export default router
