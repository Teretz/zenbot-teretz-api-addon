import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-aave-down Create open aave down
 * @apiName CreateOpenAaveDown
 * @apiGroup OpenAaveDown
 * @apiSuccess {Object} openAaveDown Open aave down's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open aave down not found.
 */
router.post('/',
  create)

export default router
