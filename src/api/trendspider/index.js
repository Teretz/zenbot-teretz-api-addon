import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /trendspider Create trendspider
 * @apiName CreateTrendspider
 * @apiGroup Trendspider
 * @apiSuccess {Object} trendspider Trendspider's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Trendspider not found.
 */
router.post('/',
  create)

export default router
