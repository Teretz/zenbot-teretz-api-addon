
export const create = ({ body }, res, next) =>
	{
		const { exec } = require("child_process");
		
		let exchange = body.exchange;
		
		let symbol = body.symbol;
		let percent = 0;
		
		if (body.percent > 0){
			percent = body.percent;
		}
		
		let action = body.action;
		console.log( exchange, symbol, percent, action)
		
		let str = 'cd /home/ubuntu/zenbot && sh ./zenbot.sh ' + action + ' ' + exchange + '.' + symbol + ' --order_type=taker';
		
		if (percent > 0 ){			str = str +	' --pct=' + percent;		}
		
		exec(str, (error, stdout, stderr) => {
			if (error) {
				console.log(`error: ${error.message}`);
				return;
			}
			if (stderr) {
				console.log(`stderr: ${stderr}`);
				return;
			}
			console.log(`stdout: ${stdout}`);
		});
		res.status(201).json(body)
			
		  
	}