import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-aave-up Create open aave up
 * @apiName CreateOpenAaveUp
 * @apiGroup OpenAaveUp
 * @apiSuccess {Object} openAaveUp Open aave up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open aave up not found.
 */
router.post('/',
  create)

export default router
