import request from 'supertest'
import { apiRoot } from '../../config'
import express from '../../services/express'
import routes, { StoreOpenAaveUp } from '.'

const app = () => express(apiRoot, routes)

let storeOpenAaveUp

beforeEach(async () => {
  storeOpenAaveUp = await StoreOpenAaveUp.create({})
})

test('POST /store-open-aave-up 201', async () => {
  const { status, body } = await request(app())
    .post(`${apiRoot}`)
    .send({ exchange: 'test', symbol: 'test', percent: 'test', action: 'test', status: 'test', message: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.exchange).toEqual('test')
  expect(body.symbol).toEqual('test')
  expect(body.percent).toEqual('test')
  expect(body.action).toEqual('test')
  expect(body.status).toEqual('test')
  expect(body.message).toEqual('test')
})
