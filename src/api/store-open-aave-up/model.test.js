import { StoreOpenAaveUp } from '.'

let storeOpenAaveUp

beforeEach(async () => {
  storeOpenAaveUp = await StoreOpenAaveUp.create({ exchange: 'test', symbol: 'test', percent: 'test', action: 'test', status: 'test', message: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = storeOpenAaveUp.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(storeOpenAaveUp.id)
    expect(view.exchange).toBe(storeOpenAaveUp.exchange)
    expect(view.symbol).toBe(storeOpenAaveUp.symbol)
    expect(view.percent).toBe(storeOpenAaveUp.percent)
    expect(view.action).toBe(storeOpenAaveUp.action)
    expect(view.status).toBe(storeOpenAaveUp.status)
    expect(view.message).toBe(storeOpenAaveUp.message)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = storeOpenAaveUp.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(storeOpenAaveUp.id)
    expect(view.exchange).toBe(storeOpenAaveUp.exchange)
    expect(view.symbol).toBe(storeOpenAaveUp.symbol)
    expect(view.percent).toBe(storeOpenAaveUp.percent)
    expect(view.action).toBe(storeOpenAaveUp.action)
    expect(view.status).toBe(storeOpenAaveUp.status)
    expect(view.message).toBe(storeOpenAaveUp.message)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
