import { Router } from 'express'
import { middleware as body } from 'bodymen'
import { create } from './controller'
import { schema } from './model'
export StoreOpenAaveUp, { schema } from './model'

const router = new Router()
const { exchange, symbol, percent, action, status, message } = schema.tree

/**
 * @api {post} /store-open-aave-up Create store open aave up
 * @apiName CreateStoreOpenAaveUp
 * @apiGroup StoreOpenAaveUp
 * @apiParam exchange Store open aave up's exchange.
 * @apiParam symbol Store open aave up's symbol.
 * @apiParam percent Store open aave up's percent.
 * @apiParam action Store open aave up's action.
 * @apiParam status Store open aave up's status.
 * @apiParam message Store open aave up's message.
 * @apiSuccess {Object} storeOpenAaveUp Store open aave up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Store open aave up not found.
 */
router.post('/',
  body({ exchange, symbol, percent, action, status, message }),
  create)

export default router
