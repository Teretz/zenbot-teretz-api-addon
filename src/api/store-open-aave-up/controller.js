import { success, notFound } from '../../services/response/'
import { StoreOpenAaveUp } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  StoreOpenAaveUp.create(body)
    .then((storeOpenAaveUp) => storeOpenAaveUp.view(true))
    .then(success(res, 201))
    .catch(next)
