import mongoose, { Schema } from 'mongoose'

const storeOpenAaveUpSchema = new Schema({
  exchange: {
    type: String
  },
  symbol: {
    type: String
  },
  percent: {
    type: String
  },
  action: {
    type: String
  },
  status: {
    type: String
  },
  message: {
    type: String
  }
}, {
  timestamps: true,
  toJSON: {
    virtuals: true,
    transform: (obj, ret) => { delete ret._id }
  }
})

storeOpenAaveUpSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,
      exchange: this.exchange,
      symbol: this.symbol,
      percent: this.percent,
      action: this.action,
      status: this.status,
      message: this.message,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('StoreOpenAaveUp', storeOpenAaveUpSchema)

export const schema = model.schema
export default model
