import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /processShorts25 Create process short 25
 * @apiName CreateProcessShort25
 * @apiGroup ProcessShort25
 * @apiSuccess {Object} processShort25 Process short 25's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Process short 25 not found.
 */
router.post('/',
  create)

export default router
