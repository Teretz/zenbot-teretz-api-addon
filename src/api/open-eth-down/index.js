import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-eth-down Create open eth down
 * @apiName CreateOpenEthDown
 * @apiGroup OpenEthDown
 * @apiSuccess {Object} openEthDown Open eth down's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open eth down not found.
 */
router.post('/',
  create)

export default router
