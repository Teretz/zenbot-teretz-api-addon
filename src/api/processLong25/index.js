import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /processLongs25 Create process long 25
 * @apiName CreateProcessLong25
 * @apiGroup ProcessLong25
 * @apiSuccess {Object} processLong25 Process long 25's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Process long 25 not found.
 */
router.post('/',
  create)

export default router
