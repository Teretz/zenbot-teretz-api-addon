import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-ada-up Create close ada up
 * @apiName CreateCloseAdaUp
 * @apiGroup CloseAdaUp
 * @apiSuccess {Object} closeAdaUp Close ada up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close ada up not found.
 */
router.post('/',
  create)

export default router
