import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-yfi-down Create open yfi down
 * @apiName CreateOpenYfiDown
 * @apiGroup OpenYfiDown
 * @apiSuccess {Object} openYfiDown Open yfi down's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open yfi down not found.
 */
router.post('/',
  create)

export default router
