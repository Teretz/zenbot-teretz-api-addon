import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /cloe-aave-down Create close aave down
 * @apiName CreateCloseAaveDown
 * @apiGroup CloseAaveDown
 * @apiSuccess {Object} closeAaveDown Close aave down's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close aave down not found.
 */
router.post('/',
  create)

export default router
