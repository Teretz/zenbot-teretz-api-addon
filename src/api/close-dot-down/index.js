import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-dot-down Create close dot down
 * @apiName CreateCloseDotDown
 * @apiGroup CloseDotDown
 * @apiSuccess {Object} closeDotDown Close dot down's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close dot down not found.
 */
router.post('/',
  create)

export default router
