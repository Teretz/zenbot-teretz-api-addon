import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-binance-longs Create open binance longs
 * @apiName CreateOpenBinanceLongs
 * @apiGroup OpenBinanceLongs
 * @apiSuccess {Object} openBinanceLongs Open binance longs's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open binance longs not found.
 */
router.post('/',
  create)

export default router
