import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-eth-up Create open eth up
 * @apiName CreateOpenEthUp
 * @apiGroup OpenEthUp
 * @apiSuccess {Object} openEthUp Open eth up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open eth up not found.
 */
router.post('/',
  create)

export default router
