import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-dot-up Create open dot up
 * @apiName CreateOpenDotUp
 * @apiGroup OpenDotUp
 * @apiSuccess {Object} openDotUp Open dot up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open dot up not found.
 */
router.post('/',
  create)

export default router
