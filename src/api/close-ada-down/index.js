import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-ada-down Create close ada down
 * @apiName CreateCloseAdaDown
 * @apiGroup CloseAdaDown
 * @apiSuccess {Object} closeAdaDown Close ada down's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close ada down not found.
 */
router.post('/',
  create)

export default router
