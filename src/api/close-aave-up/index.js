import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-aave-up Create close aave up
 * @apiName CreateCloseAaveUp
 * @apiGroup CloseAaveUp
 * @apiSuccess {Object} closeAaveUp Close aave up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close aave up not found.
 */
router.post('/',
  create)

export default router
