import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-ada-down Create open ada down
 * @apiName CreateOpenAdaDown
 * @apiGroup OpenAdaDown
 * @apiSuccess {Object} openAdaDown Open ada down's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open ada down not found.
 */
router.post('/',
  create)

export default router
