import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-xrp-up Create open xrp up
 * @apiName CreateOpenXrpUp
 * @apiGroup OpenXrpUp
 * @apiSuccess {Object} openXrpUp Open xrp up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open xrp up not found.
 */
router.post('/',
  create)

export default router
