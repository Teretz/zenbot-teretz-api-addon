import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /open-xrp-down Create open xrp down
 * @apiName CreateOpenXrpDown
 * @apiGroup OpenXrpDown
 * @apiSuccess {Object} openXrpDown Open xrp down's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Open xrp down not found.
 */
router.post('/',
  create)

export default router
