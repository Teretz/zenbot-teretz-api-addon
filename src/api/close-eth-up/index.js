import { Router } from 'express'
import { create } from './controller'

const router = new Router()

/**
 * @api {post} /close-eth-up Create close eth up
 * @apiName CreateCloseEthUp
 * @apiGroup CloseEthUp
 * @apiSuccess {Object} closeEthUp Close eth up's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Close eth up not found.
 */
router.post('/',
  create)

export default router
